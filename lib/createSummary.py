def createSummaries(orderItems, headers):
	counts = {}
	for title in headers['titles']:
		counts[title] = 0

	for item in orderItems:
		counts[orderItems[item]['item']] += orderItems[item]['qty']

	#print(counts)
	summariesToFile(counts)

def summariesToFile(counts):
	file = open("Order Summary.txt", "w+")
	for title in counts:
		file.write(str(counts[title]) + "x \t\t" + title + "\n")