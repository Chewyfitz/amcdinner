from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl.styles import Alignment, Font
from openpyxl.utils import get_column_letter

def createWorkSheet():
	wb = Workbook()

	#styling
	ws = wb.active
	ws.title = "List"

	ws.row_dimensions[1].height = str(32.25) # Title Row

	ws.column_dimensions['A'].width = str(27.71)	# Name
	ws.column_dimensions['B'].width = str(9.14)	# Reported Membership
	ws.column_dimensions['C'].width = str(11)	# Actual Membership
	ws.column_dimensions['D'].width = str(79.14)	# Order
	ws.column_dimensions['E'].width = str(9.43)	# Subtotal
	ws.column_dimensions['F'].width = str(12.57)	# Membership Cost
	ws.column_dimensions['G'].width = str(12.57)	# Membership Discount
	ws.column_dimensions['H'].width = str(9.43)	# Total

	ws.column_dimensions['J'].width = str(17.57)

	# Set Title Row
	ws.cell(column = 1, row = 1).value = "Name"
	ws.cell(column = 2, row = 1).value = "Member (Report)"
	ws.cell(column = 3, row = 1).value = "Member?"
	ws.cell(column = 4, row = 1).value = "Order"
	ws.cell(column = 5, row = 1).value = "Subtotal"
	ws.cell(column = 6, row = 1).value = "Membership Cost"
	ws.cell(column = 7, row = 1).value = "Membership Discount"
	ws.cell(column = 8, row = 1).value = "Total"
	ws.cell(column = 9, row = 1).value = "Notes"

	#Align Title Row
	tempAlignment = Alignment(horizontal = 'center', vertical = 'center', wrap_text = True)

	ws.cell(column = 1, row = 1).alignment = tempAlignment
	ws.cell(column = 2, row = 1).alignment = tempAlignment
	ws.cell(column = 3, row = 1).alignment = tempAlignment
	ws.cell(column = 4, row = 1).alignment = tempAlignment
	ws.cell(column = 5, row = 1).alignment = tempAlignment
	ws.cell(column = 6, row = 1).alignment = tempAlignment
	ws.cell(column = 7, row = 1).alignment = tempAlignment
	ws.cell(column = 8, row = 1).alignment = tempAlignment
	ws.cell(column = 9, row = 1).alignment = tempAlignment

	#Bold Title Row
	tempFont = Font(bold = True)
	ws.cell(column = 1, row = 1).font = tempFont
	ws.cell(column = 2, row = 1).font = tempFont
	ws.cell(column = 3, row = 1).font = tempFont
	ws.cell(column = 4, row = 1).font = tempFont
	ws.cell(column = 5, row = 1).font = tempFont
	ws.cell(column = 6, row = 1).font = tempFont
	ws.cell(column = 7, row = 1).font = tempFont
	ws.cell(column = 8, row = 1).font = tempFont
	ws.cell(column = 9, row = 1).font = tempFont

	return (wb, ws)

def createOrderList(orders, orderItems, headers):
	(wb, ws) = createWorkSheet()

	membershipCostCell = 	ws.cell(column = 11, row = 1)
	ws.cell(column = 10, row = 1).value = "Membership Cost"
	discountCell = 			ws.cell(column = 11, row = 2)
	ws.cell(column = 10, row = 2).value = "Member Discount"

	membershipCostCell.value = 5
	discountCell.value = 4

	#TODO: Implement this somewhere else

	#Create Reverse Header Lookup
	i = 0
	titleLookup = {}
	for title in headers['titles']:
		titleLookup[title] = i
		i+= 1

	#First, create an orderHasItems list so we can easily combine the order items
	orderHasItems = {}

	for item in orderItems:
		orderHasItems.setdefault(orderItems[item]['order'], [])
		orderHasItems[orderItems[item]['order']].append(item)
	
	#Write orders to worksheet
	offset = 0
	tempAlignment = Alignment(vertical = 'center', wrap_text = True)
	for order in orders:
		if order in orderHasItems:
			nameCell = 				ws.cell(column = 1, row = order - offset)
			rmemberCell = 			ws.cell(column = 2, row = order - offset)
			amemberCell = 			ws.cell(column = 3, row = order - offset) # Don't need to put anything in here
			orderCell = 			ws.cell(column = 4, row = order - offset)
			subtotalCell = 			ws.cell(column = 5, row = order - offset)
			membercostCell = 		ws.cell(column = 6, row = order - offset)
			memberdiscountCell = 	ws.cell(column = 7, row = order - offset)
			totalCell = 			ws.cell(column = 8, row = order - offset)

			nameCell.value = orders[order]['name']
			rmemberCell.value = orders[order]['membership']

			tempOrder = ""
			subtotalValue = 0
			first = 1
			for item in orderHasItems[order]:
				if first == 0:
					tempOrder += "\n"
				else:
					first = 0
				tempOrder += str(orderItems[item]['qty']) + "x " + orderItems[item]['item'] + " - $" + str(headers['prices'][titleLookup[orderItems[item]['item']]])
				subtotalValue += headers['prices'][titleLookup[orderItems[item]['item']]] * orderItems[item]['qty']

			orderCell.value = tempOrder
			orderCell.alignment = tempAlignment

			subtotalCell.value = subtotalValue
			membercostCell.value = "=IF("+amemberCell.coordinate+"=\"SU\", " + membershipCostCell.coordinate + ", 0)"
			memberdiscountCell.value = "=IF(OR("+amemberCell.coordinate+"=\"SU\", "+amemberCell.coordinate+"=\"Yes\"), -"+ discountCell.coordinate +",0)"
			totalCell.value = "=FLOOR(SUM("+subtotalCell.coordinate+","+membercostCell.coordinate+","+memberdiscountCell.coordinate+"),1)"

			subtotalCell.number_format = '$0.00'
			membercostCell.number_format = '$0.00'
			memberdiscountCell.number_format = '$0.00'
			totalCell.number_format = '$0.00'
		else:
			offset += 1


	wb.save("List.xlsx")