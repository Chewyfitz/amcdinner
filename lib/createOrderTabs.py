from openpyxl import Workbook
from openpyxl import load_workbook
from openpyxl import worksheet
from openpyxl.utils import get_column_letter
from openpyxl.styles import NamedStyle, Font, Border, Side, Alignment

def cmToInch(cm):
	return cm / 2.54

def createWorkSheet(numOrders):
	wb = Workbook()

	#styling
	ws = wb.active
	ws.title = "Tabs"

	ws.print_options.horizontalCentered = True

	#(left=cmToInch(0.6), right=cmToInch(0.6), top=cmToInch(1.9), bottom=cmToInch(2.4), header=cmToInch(0.8), footer=cmToInch(0.8))
	ws.page_margins.left = cmToInch(0.6)
	ws.page_margins.right = cmToInch(0.6)
	ws.page_margins.top = cmToInch(1.9)
	ws.page_margins.bottom = cmToInch(2.4)
	ws.page_margins.header = cmToInch(0.8)
	ws.page_margins.footer = cmToInch(0.8)
	
	ws.sheet_view.showGridLines = False

	for col in range (1,5):
		colLet = get_column_letter(col)
		ws.column_dimensions[colLet].width = str(24.01)

	for x in range(0, numOrders + 1):
		ws.row_dimensions[4*x + 1].height = 18.75
		ws.row_dimensions[4*x + 2].height = 15
		ws.row_dimensions[4*x + 3].height = 15
		ws.row_dimensions[4*x + 4].height = 18.75
	ws.row_dimensions[1].height = 18.75 # for some reason this isn't set

	return (wb, ws)

def placeInSheet(ws, orderList, numOrders):
	bd = Side(border_style="thin", color="000000")
	topBorder = Border(left = bd, right = bd, top = bd)
	midBorder = Border(left = bd, right = bd)
	botBorder = Border(left = bd, right = bd, bottom = bd)

	for x in range(0, numOrders):
		for y in range(0, 4):
			if(y + 4*(int(x/4)) >= numOrders):
				break
			topcell = ws.cell(column=y+1, row=4*(int(x/4))+1)
			botcell = ws.cell(column=y+1, row=4*(int(x/4))+4)

			topcell.border = topBorder
			botcell.border = botBorder

			nameCell = ws.cell(column = y+1, row = 4*(int(x/4))+2)
			orderCell = ws.cell(column = y+1, row = 4*(int(x/4))+3)

			nameCell.value = orderList[4*(int(x/4))+y]['name']
			orderCell.value = orderList[4*(int(x/4))+y]['order']
			if(orderList[x]['qty'] > 1):
				ws.cell(column = y+1, row = 4*x+3).value += "(" + str(orderList[4*(int(x/4))+ y]['qty']) + "x" + ")"
			orderCell.font = Font(bold = True, size=9)
			nameCell.font = Font(size=9)
			orderCell.border = midBorder
			nameCell.border = midBorder
			orderCell.alignment = Alignment(horizontal="center")
			nameCell.alignment = Alignment(horizontal="center")


def createTabs(orders, orderItems):
	numOrders = len(orderItems)
	(wb, ws) = createWorkSheet(numOrders)

	orderList = {}
	for item in orderItems:
		orderList[item] = {'name': orders[orderItems[item]['order']]['name'], 'order': orderItems[item]['item'], 'qty': orderItems[item]['qty']}

	placeInSheet(ws, orderList, numOrders)

	#print(orderList)
	#print(orderItems)

	wb.save("Tabs.xlsx")