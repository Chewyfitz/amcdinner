from openpyxl import Workbook
from openpyxl import load_workbook

# A function to parse the column titles
# Be careful with this if the form changes
def titleClean(string):
	# Parse Donburis and Bentos (all start with the word "Which")
	if(string.startswith('Which ')):
		i = string.index('[')
		j = string.index(']')
		return string[i+1:j]
	# Parse Gyoza (all start with the words "How many")
	# TODO: fix this to not be Magic Numbers
	elif(string.startswith('How many ')):
		strs = string.split(' ')
		i = 0
		for s in strs:
			if(s == 'gyoza'):
				break
			i += 1;
		
		if(strs[i-1] == 'seafood'):
			price = 7.8
		else:
			price = 7.5

		return (strs[i-1] + " " + strs[i] + " - ${:.2f}".format(price)).title()
	return string

def parsePrice(string):
	strs = string.split(' - $')
	if(len(strs) > 1):
		return float(strs[1])
	return 0

def parseXL(filename):
	# stuff to update if the form changes.
	# should be pretty straightforward
	sheet_name = 'Form Responses 1'
	sheet_ranges = 'A1:X1'
	num_dons = 14
	num_gyozas = 3
	total_cols = 5 + num_dons + num_gyozas +  2#25

	# First let's load the given workbook
	wb = load_workbook(filename, read_only=True)
	ws = wb[sheet_name]
	sheet_ranges = ws[sheet_ranges]

	# Now we're going to build an object for the columns
	titles = []
	prices = []
	for col in range(1, total_cols):
		temp = titleClean(ws.cell(column=col, row=1).value)
		prices.append(parsePrice(temp))
		temp = temp.split(' - $')[0]
		titles.append(temp)

	orderDict = {'titles':titles, 'prices':prices}

	# Print the columns as a sanity check
	#for title in range(1, total_cols):
	#	print(orderDict['titles'][title-1] + ' - ${:.2f}'.format(orderDict['prices'][title-1]))

	# Get number of orders
	numOrders = 0 # should be one fewer than actual number of orders
	# If you ever have more than 1000 orders you've got bigger fish to fry. Also just change the number
	for i in range(2, 1000): 
		if(ws.cell(column=1, row=i).value is None):
			numOrders = i
			break

	orders = {}
	orderItems = {}
	# Time to get some orders!
	for row in range(2, numOrders):
		orderNum = row
		time = ws.cell(row=row, column=1).value
		name = ws.cell(row=row, column=2).value
		phone = ws.cell(row=row, column=3).value
		email = ws.cell(row=row, column=4).value
		membership = ws.cell(row=row, column=5).value

		orders[orderNum] = {
		"time": time, 
		"name": name, 
		"phone": phone, 
		"email": email, 
		"membership": membership
		}
		# Parse Bentos & Gyozas here ------------------------------------------------------V
		for col in range(0, num_dons + num_gyozas):
			if(ws.cell(row=row, column= (6+col)).value != 0):
				orderItems[len(orderItems)] = {
				"order": orderNum,
				"item": titles[5+col],
				"qty": int(ws.cell(row=row, column=(6+col)).value)
				}
		# ------------------------------------------------------------------------^

	orderDict['prices'] = orderDict['prices'][5:(len(orderDict['prices'])-1)]
	orderDict['titles'] = orderDict['titles'][5:(len(orderDict['titles'])-1)]
	
	#print(orders)
	#print(orderItems)
	#print(orderDict)

	return (orders, orderItems, orderDict)