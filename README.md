# amcDinner
- AMC Dinner app. For use with Trivia Night and Welcome Back Party
- Drag the excel spreadsheet you want to use onto run.py
- alternatively, run in a command line as `run.py [triviaNightSheet.xlsx]`
## Instructions:
1. Download this script
2. Install Python3 v3.7+
3. run `pip3 install openpyxl` , or otherwise refer to https://openpyxl.readthedocs.io/en/stable/#installation (if `pip3` doesn't work, try `pip`)
4. Order Summaries are saved as "Order Summary.txt", and can be copied to an email/whatever to be sent
