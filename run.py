import sys
import json

import lib.parseXLSX
import lib.createSummary
import lib.createOrderTabs
import lib.createOrderList

if(len(sys.argv) <= 1):
	print("Expected file name")
	exit()

# Parse the given order spreadsheet
(orders, orderItems, headers) = lib.parseXLSX.parseXL(sys.argv[1])

# Create a PDF summary to send to Ramen Ya
lib.createSummary.createSummaries(orderItems, headers)

# Create a PDF tabs sheet to print out for orders
lib.createOrderTabs.createTabs(orders, orderItems)

# Create a spreadsheet for processing orders on the night
lib.createOrderList.createOrderList(orders, orderItems, headers)